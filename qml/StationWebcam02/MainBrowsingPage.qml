import "IntelInterface.js" as IntelInterface
import "Storage.js" as Storage
import "SaveandLoad.js" as SaveandLoad
import "PersistentData.js" as PersistentData
import "IntelParser.js" as IntelParser
import "IntelFavourites.js" as IntelFavourites


import QtQuick 1.1
import com.nokia.symbian 1.1
import QtMultimediaKit 1.1
import com.nokia.extras 1.0
Page {
    id: mainpagewindow
    orientationLock: PageOrientation.LockPortrait
    tools:mainbrowsingpagelayout1

    property color textColor: "#ffffff"





    HubBrowseViews{id:mainbrowser}


    ToolBarLayout {
        id:mainbrowsingpagelayout1
        visible: true
        ToolButton {
            iconSource: "toolbar-back"
            onClicked: {
                Qt.quit();
            }
        }
        ToolButton {
            iconSource: "toolbar-home"
            onClicked: {
                pageStack.push(PersistentData.FavsBrowsingPageQml);
            }
        }

        ToolButton {
            iconSource: "toolbar-menu"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked: (mainbrowsingpagemenu.status === DialogStatus.Closed) ? mainbrowsingpagemenu.open() : mainbrowsingpagemenu.close()
        }
    }

    Menu {
        id: mainbrowsingpagemenu
        visualParent: pageStack
        MenuLayout {


            MenuItem { text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml")); }

            MenuItem {
                text: qsTr("Save as favourite")
                onClicked: {
                    if (mainbrowser.getselectedcountryindex() > 0 && mainbrowser.getselectedstationindex()>0){
                        IntelFavourites.savefav(mainbrowser.getselectedcountryindex(),mainbrowser.getselectedstationindex())

                    }
                    else
                    {
                        bannerMain.text="Please select a country & resort first."
                        bannerMain.iconSource="Warning.png"

                        bannerMain.open();
                    }

                }
            }
        }
    }
    InfoBanner {
        id:bannerMain
    }




    QueryDialog {
        id: dialogmediafail
        titleText: "Streaming impossible"
        acceptButtonText: "Close"
        message: "Your connection doesn't support streaming "
    }


}


