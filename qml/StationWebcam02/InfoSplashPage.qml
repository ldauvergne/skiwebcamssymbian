import "PersistentData.js" as PersistentData
import "IntelParser.js" as IntelParser
import "IntelInterface.js" as IntelInterface

import "Storage.js" as Storage
import "SaveandLoad.js" as SaveandLoad

import QtQuick 1.1
import com.nokia.symbian 1.1


Page {
    id: infosplashpage
    orientationLock: PageOrientation.LockPortrait

    property int pops:SaveandLoad.getremainingpop()
    property string splashtext:"If you have a problem,write me an email before letting a review.<br><br>I CANNOT answer to reviews<br>on the store !<br><br>If you like this app, you can let a review on the store or contact me by clicking one of the 2 buttons below.<br><br>This message will appears "+ pops + " times.\nYou can see it again by scrolling down the settings page.";
    property string mailaddress: "skiwebcams@ovi.com"
    property int idtools:1;
    visible:true
    tools: infosplashpagetoolbarlayout1

    Component.onCompleted: if (idtools===1){
                               tools=infosplashpagetoolbarlayout1
                               infosplashpagetoolbarlayout1.visible=true;
                           }
                           else {
                               tools=infosplashpagetoolbarlayout2
                               infosplashpagetoolbarlayout2.visible=true;
                           }

    Background{
        id:background
        Component.onCompleted: background.changex(0);
    }

    Rectangle {
        id:helpScreen
        opacity:1
        width:parent.width
        height:flickAreasplashtext.contentWidth
        anchors{
            top:parent.top
            topMargin:parent.height/6
            bottom:parent.bottom
            bottomMargin:parent.height/6
        }

        color: "transparent"

        Flickable {
            id: flickAreasplashtext
            anchors{
                fill:parent

            }

            contentWidth: splashtextid.width
            contentHeight: splashtextid.height+skiwebcamslogo.height

            flickableDirection: Flickable.VerticalFlick
            clip: true
            Item{

                Image{
                    id:skiwebcamslogo
                    scale: 0.7

                    opacity: 1
                    smooth:true
                    source: "skiwebcams.png"
                }

                Text{
                    id: splashtextid
                    anchors.top:skiwebcamslogo.bottom
                    width: helpScreen.width

                    wrapMode: TextEdit.Wrap
                    textFormat: Text.RichText
                    horizontalAlignment: Text.AlignHCenter

                    font.family:  skiWebWindow.police
                    font.pixelSize: 19
                    color: "#ffffff"
                    styleColor: "#000000"

                    text:splashtext
                    style: Text.Sunken
                    font.bold: true

                }
            }
        }
    }
    ToolBarLayout {
        id:infosplashpagetoolbarlayout2
        visible: false
        ToolButton {
            id:buttonback2
            iconSource: "toolbar-previous"
            onClicked: {
                pageStack.pop();
            }
        }
        ButtonRow {
            id:infosplashbuttonrow2
            anchors{
                left:buttonback2.right
                leftMargin: infosplashpage.width/20
                right:parent.right
                rightMargin: infosplashpage.width/20
            }

            Button {
                id: splashbuttonB12
                text: "Review it"
                onClicked: Qt.openUrlExternally("http://store.ovi.com/content/232916")
            }
            Button {
                id: splashbuttonB22
                text: "Contact"
                onClicked: Qt.openUrlExternally("mailto:"+mailaddress+"?subject=Ski Webcams General Suggestion&body=Add your comment here. Please let the subject untouched.")
            }
        }
    }

    ToolBarLayout {
        id:infosplashpagetoolbarlayout1
        visible: false
        ToolButton {
            iconSource: "toolbar-back"
            onClicked: {
                Qt.quit();
            }
        }
        ButtonRow {
            id:infosplashbuttonrow1


            Button {
                id: splashbuttonB11
                text: "Review it"
                onClicked: Qt.openUrlExternally("http://store.ovi.com/content/256034")
            }
            Button {
                id: splashbuttonB21
                text: "Contact"
                onClicked: Qt.openUrlExternally("mailto:"+mailaddress+"?subject=Ski Webcams General Suggestion&body=Add your comment here. Please let the subject untouched.")
            }
        }
        ToolButton {
            iconSource: "toolbar-next"
            onClicked: {
                pageStack.replace(PersistentData.MainBrowsingPageQml)

            }
        }
    }




}

