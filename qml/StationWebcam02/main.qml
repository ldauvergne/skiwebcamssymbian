import QtQuick 1.1
import com.nokia.symbian 1.1
import "PersistentData.js" as PersistentData
import "IntelParser.js" as IntelParser
import "Storage.js" as Storage
import "SaveandLoad.js" as SaveandLoad
import "IntelFavourites.js" as IntelFavourites

PageStackWindow {
    id: skiWebWindow
    property string police: "Nokia Standard Light"
    Component.onCompleted: {
        pageStack.push(Qt.resolvedUrl("WaitingPage.qml"));
        Storage.initialize();
IntelFavourites.getfromDB();
        IntelParser.initialize();



        PersistentData.MainBrowsingPageQml=Qt.createComponent("MainBrowsingPage.qml")
PersistentData.FavsBrowsingPageQml=Qt.createComponent("FavouritesBrowsingPage.qml")

    }
}
