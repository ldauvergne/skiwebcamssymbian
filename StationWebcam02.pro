# Add more folders to ship with the application, here
folder_01.source = qml/StationWebcam02
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01





# Smart Installer package's UID
# This UID is from the protected range and therefore the package will
# fail to install if self-signed. By default qmake uses the unprotected
# range value if unprotected UID is defined for the application and
# 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian

symbian {

        TARGET.UID3 = 0x20061449

TARGET.EPOCHEAPSIZE = 0x1600000 0x8000000
TARGET = StationWebcam02_20061449

    TARGET.CAPABILITY += NetworkServices

    my_deployment.pkg_prerules += vendorinfo
    my_deployment.pkg_prerules += "(0x200346DE), 1, 1, 0, {\"Qt Quick components\"}"


    DEPLOYMENT += my_deployment


# Add dependency to Symbian components
CONFIG += qt-components

    DEPLOYMENT.display_name += Ski Webcams

    vendorinfo += "%{\"Leopold Dauvergne-FR\"}" ":\"Leopold Dauvergne-FR\""
}
# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.

    CONFIG += mobility
    MOBILITY += multimedia

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
# CONFIG += qdeclarative-boostable


# Avoid auto screen rotation
DEFINES += ORIENTATIONLOCK
# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)

qtcAddDeployment()

VERSION = 4.0.4










