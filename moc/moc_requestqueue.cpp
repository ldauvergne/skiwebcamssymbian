/****************************************************************************
** Meta object code from reading C++ file 'requestqueue.h'
**
** Created: Sat 24. Mar 00:55:14 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../component/requestqueue.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'requestqueue.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RequestQueue[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      36,   29,   13,   13, 0x0a,
      57,   13,   13,   13, 0x08,
      78,   74,   13,   13, 0x08,
     118,  112,   13,   13, 0x08,
     165,   13,   13,   13, 0x28,

       0        // eod
};

static const char qt_meta_stringdata_RequestQueue[] = {
    "RequestQueue\0\0requestReady()\0object\0"
    "addToQueue(QObject*)\0handleRequests()\0"
    "req\0adRequestFinished(QNetworkReply*)\0"
    "state\0netSessionStateChanged(QNetworkSession::State)\0"
    "netSessionStateChanged()\0"
};

const QMetaObject RequestQueue::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_RequestQueue,
      qt_meta_data_RequestQueue, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RequestQueue::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RequestQueue::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RequestQueue::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RequestQueue))
        return static_cast<void*>(const_cast< RequestQueue*>(this));
    return QObject::qt_metacast(_clname);
}

int RequestQueue::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: requestReady(); break;
        case 1: addToQueue((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 2: handleRequests(); break;
        case 3: adRequestFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 4: netSessionStateChanged((*reinterpret_cast< QNetworkSession::State(*)>(_a[1]))); break;
        case 5: netSessionStateChanged(); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void RequestQueue::requestReady()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
