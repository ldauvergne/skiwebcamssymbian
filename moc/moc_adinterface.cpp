/****************************************************************************
** Meta object code from reading C++ file 'adinterface.h'
**
** Created: Sat 24. Mar 00:55:13 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../component/adinterface.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'adinterface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AdInterface[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       1,   34, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      27,   13,   12,   12, 0x05,
      61,   12,   12,   12, 0x05,

 // methods: signature, parameters, type, tag, flags
      91,   84,   12,   12, 0x02,
     117,  111,   12,   12, 0x02,

 // properties: name, type, flags
     135,  130, 0x01495001,

 // properties: notify_signal_id
       0,

       0        // eod
};

static const char qt_meta_stringdata_AdInterface[] = {
    "AdInterface\0\0accessibility\0"
    "networkAccessibilityChanged(bool)\0"
    "networkNotAccessible()\0adItem\0"
    "requestAd(QVariant)\0adUrl\0openAd(QUrl)\0"
    "bool\0networkAccessible\0"
};

const QMetaObject AdInterface::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_AdInterface,
      qt_meta_data_AdInterface, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AdInterface::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AdInterface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AdInterface::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AdInterface))
        return static_cast<void*>(const_cast< AdInterface*>(this));
    return QObject::qt_metacast(_clname);
}

int AdInterface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: networkAccessibilityChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: networkNotAccessible(); break;
        case 2: requestAd((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 3: openAd((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = networkAccessible(); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::WriteProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void AdInterface::networkAccessibilityChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void AdInterface::networkNotAccessible()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
